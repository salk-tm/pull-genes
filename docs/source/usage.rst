Usage
=====

.. role:: zsh(code)
   :language: zsh

Usage text
----------

.. code-block:: zsh

   usage: pull_seq.py [-h] [--type <type>] [--tag <tag>]
                      [--write-fasta <output.fasta>] [--rna] [--force-strand]
                      [--one-based]
                      <genome.fasta> <genes.{gff,gff3,bed}[.gz]>
                      [<VALUE> [<VALUE> ...]]
   
   Pull nucleotide sequences
   
   positional arguments:
     <genome.fasta>        FASTA file containing genome sequence
     <genes.{gff,gff3,bed}[.gz]>
                           GFF3 or BED file containing gene coordinates
     <VALUE>               GFF3 values to pull
   
   optional arguments:
     -h, --help            show this help message and exit
     --type <type>         Type of record to read from the GFF3 file [gene]
     --tag <tag>           GFF3 tag to pull [ID]
     --write-fasta <output.fasta>
                           Write FASTA file to disk instead of printing
     --rna                 FASTA is RNA
     --force-strand        force strandedness

import pytest
from pull_seq.pull_seq import pull_seq

def test_pull_seq(fasta, gff, gene_ids, gene_seqs):
    assert pull_seq(fasta, gff, gene_ids[0]) == gene_seqs[0]
    assert pull_seq(fasta, gff, gene_ids[1]) == gene_seqs[1]
    assert pull_seq(fasta, gff, gene_ids[0], gene_ids[1]) == gene_seqs[0] + gene_seqs[1]

def test_pull_mrna(fasta, gff, transcript_id, gene_ids, gene_seqs):
    assert pull_seq(fasta, gff, transcript_id, type='mRNA').split('\n')[1] == gene_seqs[0].split('\n')[1]
    assert pull_seq(fasta, gff, gene_ids[0], type='mRNA', tag='Parent') == gene_seqs[0]
    
# def test_pull_exons(fasta, gff, transcript_id, exon_seqs):
#     assert pull_seq(fasta, gff, transcript_id, type='exon', tag='Parent') == exon_seqs

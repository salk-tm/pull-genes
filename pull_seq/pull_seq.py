#===============================================================================
# pull_seq.py
#===============================================================================

"""Pull nucleotide sequences from FASTA using GFF3 or BED"""

# Imports ======================================================================

from argparse import ArgumentParser
from pathlib import PosixPath
from pybedtools import BedTool
import gzip
import gff2bed




# Functions ====================================================================

def pull_seq(fasta: str, coordinates_file: str, *values, type: str = 'gene',
    tag: str = 'ID', force_strand: bool = False, rna: bool = False,
    one_based: bool = False):
    """Pull gene sequences from a FASTA file and GFF3 coordinates

    Parameters
    ----------
    fasta : str
        path to FASTA file
    coordinates_file : str
        path to GFF3 or BED file
    values
        GFF3 values to pull
    type : str
        type of GFF3 record to read [gene]
    tag : str
        GFF3 key for gene ID value [ID]
    force_strand: bool
        force strandedness
    rna : bool
        fasta is RNA
    
    Returns
    -------
    str
        a FASTA file containing gene sequences
    """

    if any(coordinates_file.endswith(x) for x in ('.bed', '.bed.gz')):
        if values:
            value_set = set(values)
            coordinates = BedTool(x for x in BedTool(coordinates_file) if x.name in value_set)
        else:
            coordinates = BedTool(coordinates_file)
    else:
        if values:
            value_set = set(values)
            coordinates = BedTool(tuple(x for x in gff2bed.convert(
                gff2bed.parse(coordinates_file, type=type), tag=tag)
                if x[3] in value_set)).sort()
        else:
            coordinates = BedTool(
                tuple(gff2bed.convert(gff2bed.parse(coordinates_file, type=type), tag=tag))).sort()
    coordinates = coordinates.sequence(fi=fasta, name=True, s=force_strand, rna=rna)
    with open(coordinates.seqfn) as seqfn:
        return seqfn.read()


def parse_arguments():
    parser = ArgumentParser(description='Pull nucleotide sequences')
    parser.add_argument('fasta', metavar='<genome.fasta>',
        help='FASTA file containing genome sequence')
    parser.add_argument('coordinates', metavar='<genes.{gff,gff3,bed}[.gz]>',
        help='GFF3 or BED file containing gene coordinates')
    parser.add_argument('values', metavar='<VALUE>', nargs='*',
        help='GFF3 values to pull')
    parser.add_argument('--type', metavar='<type>', default='gene',
        help='Type of record to read from the GFF3 file [gene]')
    parser.add_argument('--tag', metavar='<tag>', default='ID',
        help='GFF3 tag to pull [ID]')
    parser.add_argument('--write-fasta', metavar='<output.fasta>',
        help='Write FASTA file to disk instead of printing')
    parser.add_argument('--rna', action='store_true', help='FASTA is RNA')
    parser.add_argument('--force-strand', action='store_true',
        help='force strandedness')
    # parser.add_argument('--one-based', action='store_true',
    #     help='Use one-based (GFF style) coordinates in the header line of the output FASTA')
    return parser.parse_args()


def main():
    args = parse_arguments()
    accepted_suffixes = ('.gff', '.gff3', '.bed','.gff.gz', '.gff3.gz', '.bed.gz')
    if not any(args.coordinates.endswith(x) for x in accepted_suffixes):
        raise RuntimeError(
            f"Coordinates file must have an accepted suffix: {', '.join(accepted_suffixes)}")
    else:
        fasta_output = pull_seq(args.fasta, args.coordinates, *args.values,
            type=args.type, tag=args.tag,
            force_strand=args.force_strand, rna=args.rna, one_based=False)
        if args.write_fasta:
            with open(args.write_fasta, 'w') as f:
                f.write(fasta_output)
        else:
            print(fasta_output, end='')



# Execute ======================================================================

if __name__ == '__main__':
    main()

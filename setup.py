import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='pull-seq',
    version='1.0.0', # Don't forget to match with version.py
    author='Anthony Aylward, Ying Sun',
    author_email='aaylward@salk.edu',
    description='Pull nucleotide sequences from FASTA file using GFF3 or BED',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/salk-tm/pull-seq',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
    ],
    install_requires=['pybedtools'],
    entry_points={
        'console_scripts': ['pull-seq=pull_seq.pull_seq:main']
    },
    include_package_data=True
)
